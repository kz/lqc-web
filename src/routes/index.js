import React from   'react';
import { Route, Switch } from 'react-router-dom';
import Index from '../components/Index'


const Routes = (props) => { 
	return(
	  <Switch>
	    <Route path="/" component={Index} exact />
	  </Switch>
	);
}

export default Routes;