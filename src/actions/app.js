import $ from 'jquery'
// import { socket } from 'js/io.js'

export const REQUEST_FILES = 'REQUEST_FILES'
export const REQUEST_FILES_SUCCESS = 'REQUEST_FILES_SUCCESS'
export const REQUEST_FILES_ERROR = 'REQUEST_FILES_ERROR'
export const CLEAR_FILES = 'CLEAR_FILES'
export const SET_CALENDAR_DATE = 'SET_CALENDAR_DATE'

export const SOCKET_CONNECT = 'SOCKET_CONNECT'
export const SOCKET_DISCONNECT = 'SOCKET_DISCONNECT'

export const EP = 'http://localhost:5001'
// export const EP = 'https://rest.parser.milnomada.io'
const Bearer = ''

// export const IO = socket


/**
 * Get all sites
 */
export const getFiles = (d) => (dispatch) => {

  const data = d ? {date: d} : {}

  dispatch({type: REQUEST_FILES, ts: Date.now()})

  const mySuccessCallback = res => {
    dispatch({
      type: REQUEST_FILES_SUCCESS,
      data: res.files,
      ts: Date.now()
    })
  }

  const myErrorCallback = (res, status, err) => {
    console.log(res, status, err)
    dispatch({
      type: REQUEST_FILES_ERROR,
      err: err,
      ts: Date.now()
    })
  }

  $.ajax({
    url: `${EP}/files`,
    method: 'POST',
    data: JSON.stringify(data),
    dataType: "json",
    success: mySuccessCallback,
    error: myErrorCallback,
    beforeSend: function (xhr) {
      xhr.setRequestHeader("Content-Type", "application/json")
      // xhr.setRequestHeader("Authorization", `Bearer ${Bearer}`)
    }
  });
}


export const clearFiles = () => (dispatch) => {
  dispatch({type: CLEAR_FILES, ts: Date.now()})
}

export const setDate = (d) => (dispatch) => {
  dispatch({type: SET_CALENDAR_DATE, dt: d, ts: Date.now()})
}

/**
 * Get one post would be defined as follows
 */
export const createSite = (site) => (dispatch) => {
  dispatch({type: "", ts: Date.now()})

  const mySuccessCallback = res => {
    dispatch({
      type: "",
      data: res.site, 
      ts: Date.now()
    })
  }

  const myErrorCallback = (res, status, err) => {
    dispatch({
      type: "",
      err: err,
      ts: Date.now()
    })
  }

  $.ajax({
    url: `${EP}/site`,
    method: 'POST',
    data: JSON.stringify(site),
    dataType: "json",
    success: mySuccessCallback,
    error: myErrorCallback,
    beforeSend: function (xhr) {
      xhr.setRequestHeader("Content-Type", "application/json")
      xhr.setRequestHeader("Authorization", `Bearer ${Bearer}`)
    }
  });
}

/**
 * Get one post would be defined as follows
 */
export const updateSite = (id, fields) => (dispatch) => {
  dispatch({type: "", ts: Date.now()})

  const mySuccessCallback = res => {
    dispatch({
      type: "",
      values: res.fields,
      id: id,
      ts: Date.now()
    })
  }

  const myErrorCallback = (res, status, err) => {
    dispatch({
      type: "",
      err: err,
      ts: Date.now()
    })
  }

  $.ajax({
    url: `${EP}/site/${id}`,
    method: 'PUT',
    data: JSON.stringify(fields),
    dataType: "json",
    success: mySuccessCallback,
    error: myErrorCallback,
    beforeSend: function (xhr) {
      xhr.setRequestHeader("Content-Type", "application/json")
      xhr.setRequestHeader("Authorization", `Bearer ${Bearer}`)
    }
  });
}

/**
 * Delete site
 */
export const deleteSite = (id) => (dispatch) => {

  dispatch({type: "", ts: Date.now()})

  const mySuccessCallback = res => {
    dispatch({
      type: "",
      data: id,
      ts: Date.now()
    })
  }

  const myErrorCallback = (res, status, err) => {
    console.log(res, status, err)
    dispatch({
      type: "",
      err: err,
      ts: Date.now()
    })
  }

  $.ajax({
    url: `${EP}/site/${id}`,
    method: 'DELETE',
    dataType: "json",
    success: mySuccessCallback,
    error: myErrorCallback,
    beforeSend: function (xhr) {
      xhr.setRequestHeader("Content-Type", "application/json")
      xhr.setRequestHeader("Authorization", `Bearer ${Bearer}`)
    }
  });
}

/**
 * Get one post would be defined as follows
 */
export const getSite = (id) => (dispatch) => {
}

/**
 * Get one post would be defined as follows
 */
export const testRun = (url) => (dispatch, getState) => {
  dispatch({type: "", ts: Date.now()})

  var state = getState()
  if(!state.app.sid) {
    console.log("Io not connected")
    return
  }

  const mySuccessCallback = res => {
    dispatch({
      type: "",
      data: res.site, 
      ts: Date.now()
    })
  }

  const myErrorCallback = (res, status, err) => {
    console.log(res, status, err)
    dispatch({
      type: "",
      err: err,
      ts: Date.now()
    })
  }

  $.ajax({
    url: `${EP}/run`,
    method: 'POST',
    data: JSON.stringify({url: url, sid: state.app.sid}),
    dataType: 'json',
    success: mySuccessCallback,
    error: myErrorCallback,
    beforeSend: function (xhr) {
      xhr.setRequestHeader("Content-Type", "application/json")
      xhr.setRequestHeader("Authorization", `Bearer ${Bearer}`)
    }
  });
}

/**
 * Get one post would be defined as follows
 */
export const runSiteCallback = (sid, data) => (dispatch) => {
  dispatch({type: "", sid: sid, data: data, ts: Date.now()})
}

/**
 * Get one post would be defined as follows
 */
export const socketConnect = (sid) => (dispatch) => {
  dispatch({type: SOCKET_CONNECT, id: sid, ts: Date.now()})
}

export const socketDisconnected = () => (dispatch) => {
  dispatch({type: SOCKET_DISCONNECT, ts: Date.now()})
}

/**
 * Get one post would be defined as follows
 */
export const validateXpath = (xpath, fn) => (dispatch) => {
  dispatch({type: "", ts: Date.now()})

  const mySuccessCallback = res => {
    dispatch({
      type: "",
      ts: Date.now()
    })
    fn(res, null)
  }

  const myErrorCallback = (res, status, err) => {
    console.log(res, status, err)
    dispatch({
      type: "",
      err: res.message || err,
      ts: Date.now()
    })
    fn(null, res.message || err)
  }

  $.ajax({
    url: `${EP}/run/validate`,
    method: 'POST',
    data: JSON.stringify({xpath: xpath}),
    dataType: "json",
    success: mySuccessCallback,
    error: myErrorCallback,
    beforeSend: function (xhr) {
      xhr.setRequestHeader("Content-Type", "application/json")
      xhr.setRequestHeader("Authorization", `Bearer ${Bearer}`)
    }
  });
}
