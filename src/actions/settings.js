export const SET_SETTING = 'SET_SETTING'


export const setSetting = (key, value) => (dispatch) => {
  dispatch({
    type: SET_SETTING,
    key: key,
    value: value,
    ts: Date.now()
  })
}
