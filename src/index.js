import React from 'react';
import { render } from 'react-dom'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { save, load } from "redux-localstorage-simple"

import Reducer from './reducers'
import App from './containers/App';

const middleware = [thunk],
      ns = "sitepatterns"
      ;
middleware.push(save({ namespace: ns }))

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger())
}

const middlewareStore = applyMiddleware( ...middleware )(createStore);
const store = middlewareStore(Reducer, load({ namespace: ns }));

render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="*" component={App} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);
