import React from 'react';
import PropTypes from 'prop-types'
import { Player, PosterImage, BigPlayButton, ControlBar, VolumeMenuButton } from 'video-react';
import 'video-react/dist/video-react.css';
import moment from 'moment'
import $ from 'jquery'


PosterImage.propTypes = {
  poster: PropTypes.string,
}

const MAX_HEIGHT_VIDEO_FRAME = 470
const MAX_RETRIES = 3

const urlify = (text) => {
  var urlRegex = /(https?:\/\/[^\s]+)/g;
  return text.replace(urlRegex, function(url) {
    return '<a href="' + url + '" rel="noopener" target="_blank">' + url + '</a>';
  })
}

class Post extends React.Component {
  static propTypes = {
    posts: PropTypes.array,
    isFetching: PropTypes.bool
  }

  constructor(props){
    super(props);
    this.player = null
    this.file = null
    this.state = {
      data: null,
      video: null,
      picture: null,
      showReload: false,
      isReloading: false,
      notAvailable: false
    }
  }

  fetchResource(file, effort) {
    var _this = this
    fetch(file).then(response => {
      if (!response.ok) {
        throw new Error("HTTP error " + response.status);
      }
      return response.json();
    })
    .then(json => {
      _this.setState({data: json})
      _this.setState({isReloading: false})
    })
    .catch(function () {
      console.log(`Error fetching ${file} ${effort}`)
      if(effort <= MAX_RETRIES)
        _this.fetchResource(file, effort + 1)
      else {
        if(_this.state.showReload)
          _this.setState({notAvailable: true, isReloading: false})
        else
          _this.setState({showReload: true})
      }
    })
  }

  reload() {
    this.setState({isReloading: true})
    this.fetchResource(this.file, 1)
  }

  componentDidMount() {
    var jsonFile = this.props.site.files.filter(e => e.indexOf("json") > -1)
    var video = this.props.site.files.filter(e => e.indexOf("mp4") > -1)
    var picture = this.props.site.files.filter(e => e.indexOf("jpg") > -1 || e.indexOf("png") > -1)

    if(jsonFile.length > 0) {
      this.file = jsonFile[0]
    }

    if(video.length > 0) {
      this.setState({video: video[0]})
      this.video = video[0]
    }

    if(picture.length > 0) {
      this.setState({picture: picture[0]})
      this.picture = picture[0]
    }

    this.fetchResource(jsonFile, 1)
  }

  render() {
    if($('video.video-react-video')) {
      const videoElem = $('video.video-react-video')
      $.each(videoElem, (i, e) => {
        $(e).on('loadeddata', (evt) => {
          const target = evt.target
          if($(target).attr("data-panned") === "1") {
            return
          }
          // console.log("Loaded!!!", $(target).height())
          if($(target).height() > MAX_HEIGHT_VIDEO_FRAME) {
            // console.log("Pannning video and button")
            var pan = ($(target).height() - MAX_HEIGHT_VIDEO_FRAME) / 2
            $(target).css({'margin-top': `${-pan}px`})
            $(target).parent().find('.video-react-big-play-button').css({'margin-top': `${-pan}px`})
            $(target).parent().find('.video-react-control-bar').css({'bottom': `${pan * 2}px`})
            $(target).attr("data-panned", "1")
          }
        })
      })
    }

    const text = this.state.data ? urlify(this.state.data.message) : ""
    const pos = this.props.pos
    const dt = this.state.data ? moment.parseZone(this.state.data.date).format("LLLL") : "" 

    return (<div className="post" style={this.state.showReload ? {minHeight: "90px"} : {}} data-date={this.state.data ? this.state.data.date : ""}>
      <div style={{backgroundImage: `url(/img/pattern-${pos % 14}.svg)`, backgroundPosition: "100%", opacity: "0.1"}} className="post-bg"></div>
      { this.state.showReload && <div className="post-actions">
        <span className="action" title="Reload post" onClick={() => this.reload()}><i className={this.state.isReloading ? "fa fa-refresh fa-spin" : "fa fa-refresh"}></i></span>
      </div>}
      { this.state.showReload && this.state.notAvailable && !this.state.isReloading && <div className="title">The resource is not available</div> }
      { this.state.data && <div className="post-head">
        <div className="text"><span className="id">{this.state.data.id} </span>{dt}</div>
        
        <div className="title" dangerouslySetInnerHTML={{__html: text}}></div>
        <div className="details">
          <span><i className="fa fa-eye"></i> <span>{this.state.data.views}</span></span>
          <span><i className="fa fa-share-square"></i> <span>{this.state.data.forwards}</span></span>
        </div>
      </div> }
      <div className="post-body">
        { this.state.picture && <div className="post-img-bg"><img alt="post" src={this.state.picture} title="" width="100%" /></div>}
        { this.state.video && this.state.data && <div className="post-video-frame">
          <Player className={`player-${this.state.data.id}`} width="640" height="320" controls ref={(player) => { this.player = player }}>
            <source src={this.state.video} type="video/mp4"></source>
            <BigPlayButton position="center" />
            <ControlBar autoHide={false} className="">
              <VolumeMenuButton vertical />
            </ControlBar>
          </Player>
          </div>
        }
      </div>
      <div className="post-post"></div>
    </div>)
  }
}

export default Post
