import React from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getFiles, clearFiles, setDate } from '../actions/app'
import { setSetting } from '../actions/settings'

import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css'
import Post from './Post'
import moment from 'moment'


class Index extends React.Component {

	static propTypes = {
    posts: PropTypes.array,
    isFetching: PropTypes.bool
  }

  constructor(props){
    super(props);
    this.state = {
      date: new Date(),
      showCalendar: false,
      showSettings: false
    }
  }

  componentDidMount() {
    if(this.props.app.ts < Date.now() - 120000) {
      this.props.fetch();
    }
    this.setState({date: this.props.app.calendarDate})

  }

  /*componentWillReceiveProps(props) {
    if(this.props.app.data.length < props.app.data.length) {

    }
  }*/

  fetchPrevious() {
    var dt = moment().subtract(1, 'days')
    this.props.fetch(dt.format());
  }
  
  toActive(a, b) {
    this.props.update(a, b)
  }

  onChange(date) {
    this.setState({ date: date })
    this.props.clear()
    this.props.setDate(date)
    
    setTimeout(() => {
      var dt = moment(date)
      this.props.fetch(dt.format());
    }, 300)

    setTimeout(() => {
      this.toggleCalendar()
    }, 300)
  }

  toggleCalendar() {
    if(!this.state.showCalendar === true) {
      this.setState({showCalendar: !this.state.showCalendar, showSettings: false})
    } else {
      this.setState({showCalendar: !this.state.showCalendar})
    }
  }

  toggleSettings() {
    if(!this.state.showSettings === true) {
      this.setState({showSettings: !this.state.showSettings, showCalendar: false})
    } else {
      this.setState({showSettings: !this.state.showSettings})
    }
  }

  render() {
  	return(
      <div className="site-index">
        <div className="site-actions">
          <ul>
            <li>
              <span className={this.state.showCalendar ? "action on" : "action"} title="Show calendar" onClick={() => {this.toggleCalendar()}}><i className="fa fa-calendar"></i></span>
              { this.state.showCalendar && <Calendar
                onChange={(d) => {}}
                onClickDay={(d) => {this.onChange(d)}}
                value={this.state.date}
              /> }
            </li>
            <li><span className="action" title="Clear posts" onClick={() => this.props.clear()}><i className="fa fa-trash"></i></span></li>
            <li><span className={this.state.showSettings ? "action on" : "action"} title="Settings" onClick={() => {this.toggleSettings()}}><i className="fa fa-cog"></i></span></li>
          </ul>
        </div>
        <div className="post-list">
          { this.state.showSettings && <div className="post settings">
            <div className="settings-bg"></div>
            <div className="post-head">
              <div className="title">Settings</div>
            </div>
            <div className="post-body">
              <div className="settings-row">
                <input type="checkbox" checked={this.props.settings.scrollTopLoad} onClick={() => { this.props.setSetting("scrollTopLoad", !this.props.settings.scrollTopLoad) }} />
                <span className="text">Load posts when Scroll reaches Top</span>
              </div>
              <div className="settings-row">
                <input type="checkbox" checked={this.props.settings.scrollBottomLoad} onClick={() => { this.props.setSetting("scrollBottomLoad", !this.props.settings.scrollBottomLoad) }} />
                <span className="text">Load posts when Scroll reaches Bottom</span>
              </div>
            </div>
          </div> }
          { this.props.files.length > 0 && this.props.files.map((e, i) => <Post key={e.id} pos={i + 1} site={e} />)}
          { this.props.files.length === 0 && !this.props.app.isFetching && <div className="post">Still no posts for today. Otherwise, you may try for the <span style={{fontWeight: "bold", cursor: "pointer", color: "#428bca"}} onClick={() => {this.fetchPrevious()}}>previous</span> day,
            or choose any date from the <span style={{fontWeight: "bold", cursor: "pointer", color: "#428bca"}} onClick={() => {this.toggleCalendar()}}>calendar.</span></div> }
          { this.props.app.isFetching && <div className="post">Loading posts <img alt="loader" src="/img/loader.gif" style={{height: "1em"}} /> </div> }
        </div>
      </div>
  	)
  }
}

const mapStateToProps = (state, props) => {
  return {
    app: state.app,
    files: state.app.data,
    settings: state.settings
  }
}

const mapDispatchToProps = (dispatch) => {  
  return { 
    fetch: (e) => {
      dispatch(getFiles(e))
    },
    clear: (e) => {
      dispatch(clearFiles())
    },
    setDate: (d) => {
      dispatch(setDate(d))
    },
    setSetting: (k, v) => {
      dispatch(setSetting(k, v))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)
