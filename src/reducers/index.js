import { combineReducers } from 'redux'
import app from './app'
import settings from './settings'

const Reducer = combineReducers({app, settings})

export default Reducer
