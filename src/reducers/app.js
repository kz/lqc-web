/* eslint-disable no-loop-func */
import {
  REQUEST_FILES,
  REQUEST_FILES_SUCCESS,
  REQUEST_FILES_ERROR,
  CLEAR_FILES,
  SET_CALENDAR_DATE,
  SOCKET_CONNECT,
  SOCKET_DISCONNECT
} from '../actions/app'

const appInitState = {
  isFetching: false,
  isFetchingTest: false,
  isUpdating: false,
  isCreating: false,
  isDeleting: false,
  hasErrors: false,
  errors: [],
  data: [],
  calendarDate: null,
  size: 0,
  sid: null,
  ts: null
}

const app = (state = appInitState, action) => {
  var i, newData, some

  if(action.type.indexOf("INIT") > -1) {
    return {
      ...appInitState,
      ts: state.ts,
      data: state.data,
      size: state.size
    }
  }

  switch (action.type) {
    case REQUEST_FILES:
      return {
        ...state,
        isFetching: true
      }
    case REQUEST_FILES_SUCCESS:
      /**
       * Translate json text (stored in db)
       * to json object
       */
      newData = []
      for(i=0; i<action.data.length; i++) {
        some = state.data.filter(e => e.id === action.data[i].id)
        if(some.length === 0) {
          newData.push(action.data[i])
        }
      }
      return {
        ...state,
        isFetching: false,
        data: state.data.concat(newData),
        size: newData.length,
        ts: Date.now()
      }
    case CLEAR_FILES:
      return {
        ...state,
        data: [],
        size: 0,
        ts: action.ts
      }
    case SET_CALENDAR_DATE:
      return {
        ...state,
        calendarDate: action.dt
      }
    case REQUEST_FILES_ERROR:
      return {
        ...state,
        isFetching: false,
        hasErrors: true,
        errors: action.err
      }
    case SOCKET_CONNECT:
      return {
        ...state,
        sid: action.id,
        hasErrors: false
      }
    case SOCKET_DISCONNECT:
      return {
        ...state,
        sid: null,
        hasErrors: false
      }
    default:
      return state
  }
}

export default app
