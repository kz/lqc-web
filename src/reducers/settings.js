import {
  SET_SETTING
} from '../actions/settings'

const settingsState = {
  scrollTopLoad: false,
  scrollBottomLoad: false,
  ts: null
}

const settings = (state = settingsState, action) => {

  if (action.type.indexOf("INIT") > -1) {
    return {
      ...state,
    }
  }
  switch (action.type) {
    case SET_SETTING:
      var obj = {}
      obj[action.key] = action.value
      return {
        ...state,
        ...obj
      }
    default:
      return state
  }
      
}

export default settings
