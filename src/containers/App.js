import React from 'react';
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import Routes from '../routes'
import { getFiles, clearFiles } from '../actions/app'
import $ from 'jquery'
import moment from 'moment'


class App extends React.Component {

  static propTypes = {
    data: PropTypes.array,
    size: PropTypes.number,
    isFetching: PropTypes.bool
  }

  constructor(props){
    super(props);
    this.state = {
      sid: null,
      enableConnection: false
    }
  }

  componentDidMount() {
    /*
    var ref = this

    IO.on('connect', function() {
      if(debug)
        console.log('connect >>', IO, IO.id, IO.sessionId, IO.json.id);
      ref.setState({sid: IO.id})

      // if(ref.state.enableConnection)
      ref.props.connect(IO.id)
    })
    
    IO.on('ihsc', function(data) {
      if(debug)
        console.log('ihsc >>', IO, IO.id, data);
    })

    IO.on('x-end-run', function(data) {
      if(debug)
        console.log('x-end-run >>', IO, IO.id, data);
      ref.props.callback(IO.id, data.post)
    })

    IO.emit('ih', {uuid: "uuid"});

    IO.on('error', function() {
      if(debug)
        console.log("socket disconnected")
    })
    IO.on('reconnect', (attemptNumber) => {
      if(debug)
        console.log("socket reconnect " + attemptNumber)
      
      // if(ref.state.enableConnection)
      ref.props.connect(IO.id)
    })

    IO.on('reconnect_error', (error) => {
      if(debug)
        console.log("socket reconnect " + error)
      this.props.disconnect()
    })
    */
  }

  handleScroll(e){
    const target = e.target
    var dt, mdt, elem, strFormat
    if(target.scrollHeight - target.scrollTop === target.clientHeight && this.props.settings.scrollBottomLoad) {
      console.log("End of scroll")
      elem = $('.post:last')
      dt = elem.attr('data-date')
      mdt = moment.parseZone(dt).subtract(1, 'days')
      strFormat = mdt.format()
      while(strFormat.indexOf("Invalid") > -1) {
        elem = elem.prev('.post')
        dt = elem.attr('data-date')
        mdt = moment.parseZone(dt).subtract(1, 'days')
        strFormat = mdt.format()
      }
      console.log(strFormat)
      this.props.fetch(strFormat)
    }
    if(target.scrollTop === 0 && this.props.settings.scrollTopLoad) {
      console.log("Scroll at the top")
      elem = $('.post:first')
      dt = elem.attr('data-date')
      mdt = moment.parseZone(dt).add(1, 'days')
      strFormat = mdt.format()
      while(strFormat.indexOf("Invalid") > -1) {
        elem = elem.next('.post')
        dt = elem.attr('data-date')
        mdt = moment.parseZone(dt).add(1, 'days')
        strFormat = mdt.format()
      }
      console.log(strFormat)
      if(! mdt.isSame(moment(), "day")) {
        this.props.clear()
        this.props.fetch(strFormat)
      }
    }

    console.log("Scroll")
  }

  render() {
    return (
      <div>
        <div className="body-bg"></div>
        <div className="scroll-wrapper" onScroll={(e) => this.handleScroll(e)}>
        <div className="app row">
          <div className="content">
            <Routes />
          </div>
        </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (reducer, props) => {
  return {
    data: reducer.app.data,
    size: reducer.app.size,
    settings: reducer.settings,
    isFetching: reducer.app.isFetching
  }
}

const mapDispatchToProps = (dispatch) => {  
  return {
    fetch: (d) => {
      dispatch(getFiles(d))
    },
    clear: (e) => {
      dispatch(clearFiles())
    },
    /* connect: (s) => {
      dispatch(socketConnect(s))
    },
    callback: (i, d) => {
      dispatch(runSiteCallback(i, d))
    },
    disconnect: () => {
      dispatch(socketDisconnected())
    } */
  }
}

/**
 * connect
 * 
 * THe application is connected to hear about Redux store changes.
 * Shown example, posts count in header.
 */
export default connect(mapStateToProps, mapDispatchToProps)(App)
