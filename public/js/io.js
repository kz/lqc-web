// var host = 'http://localhost:3333'
var host = 'https://s.parser.milnomada.io'
var socket = io(host, {
    transports: ["websocket"],
    reconnection: true
})  //, {path:'/', namespace:'socket'});

export { socket }
