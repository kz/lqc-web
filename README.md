# LQC Telegram backup

A web app to serve Telegram backed-up content from La Quinta Columna

### Abstract

This web app serves content served by an API, which has been fetched 
from telegram, using the telegram API, stored in an independent infrastructure,
and available to be used by authorized clients.

